package com.epam.enrolleesystem.annotation;

import java.lang.annotation.*;

@Target({ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuthenticatedUser {

    boolean errorOnInvalidType() default false;
    String expression() default "";
}
