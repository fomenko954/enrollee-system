package com.epam.enrolleesystem.util;

import com.epam.enrolleesystem.exception.EnrollRuntimeException;

public final class AssertUtils {

    private AssertUtils() {
        throw new UnsupportedOperationException();
    }

    public static <T extends EnrollRuntimeException> void assertNotNull(Object obj, String message, ExceptionSupplier<T> supplier) {
        if(obj == null) {
            throw supplier.throwException(message);
        }
    }
}
