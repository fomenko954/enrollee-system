package com.epam.enrolleesystem.util;

import com.epam.enrolleesystem.exception.FileStorageException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FileUtils {

    public static void createDirectory(Path storageLocation) {
        try {
            Files.createDirectories(storageLocation);
        }  catch (IOException e) {
            throw new FileStorageException("Cannot create a directory to store uploaded certificate.");
        }
    }

}
