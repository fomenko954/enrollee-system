package com.epam.enrolleesystem.util;

@FunctionalInterface
public interface ExceptionSupplier<T> {

    T throwException(String massage);
}
