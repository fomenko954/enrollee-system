package com.epam.enrolleesystem.util;

import com.epam.enrolleesystem.domain.ErrorResponse;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.stream.Collectors;

public final class ApiUtils
{

    private ApiUtils()
    {
        throw new UnsupportedOperationException();
    }

    public static ErrorResponse<Map<String, String>> mapToErrorResponse(BindingResult bindingResult)
    {
        Map<String, String> errorFields = bindingResult.getFieldErrors().stream()
                .collect(Collectors.toMap(FieldError::getField, DefaultMessageSourceResolvable::getDefaultMessage));


        return ErrorResponse.<Map<String, String>>builder()
                .message("The data was filled incorrect.")
                .additionalInfo(errorFields)
                .currentDate(LocalDateTime.now())
                .httpStatus(HttpStatus.BAD_REQUEST)
                .build();
    }

}
