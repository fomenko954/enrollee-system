package com.epam.enrolleesystem.filter;

import com.epam.enrolleesystem.domain.JwtUserDetails;
import com.epam.enrolleesystem.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@Component
public class UserBlockFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if(request.getRequestURI().startsWith("/api")) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            Object principal = null;
            User user = null;
            if (Objects.nonNull(authentication)) {
                principal = authentication.getPrincipal();
            }

            if (principal instanceof JwtUserDetails) {
                user = ((JwtUserDetails) principal).getUser();
            }

            if (user != null && !user.getIsBlocked()) {
                filterChain.doFilter(request, response);
            } else {
                response.sendError(403, "Your account is blocked.");
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }
}
