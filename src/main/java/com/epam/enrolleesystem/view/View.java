package com.epam.enrolleesystem.view;

public class View
{
    public interface ResponseView {}

    public interface AuthenticationResponseView {}

    public interface FacultySubjectResponseView extends ResponseView {}

    public interface SubjectFacultyResponseView extends ResponseView {}

    public interface UserFacultyResponseView extends ResponseView {}

    public interface FacultyUserResponseView extends ResponseView {}

    public interface UserSubjectResponseView extends ResponseView {}

    public interface CompliantResponseView extends ResponseView {}

    public interface MarkResponseView extends ResponseView {}
}
