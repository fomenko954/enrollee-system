package com.epam.enrolleesystem.config;

import com.epam.enrolleesystem.domain.HttpStatus;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private static final ResponseMessage[] DEFAULT_STATUSES = new ResponseMessage[] {
            contractMessage(HttpStatus.UNAUTHORIZED),
            contractMessage(HttpStatus.FORBIDDEN),
            contractMessage(HttpStatus.NOT_FOUND),
            contractMessage(HttpStatus.INTERNAL_SERVER_ERROR)
    };

    @Bean
    public SecurityConfiguration security() {
        return SecurityConfigurationBuilder.builder().scopeSeparator(",")
                .additionalQueryStringParams(null)
                .useBasicAuthenticationWithAccessCodeGrant(false).build();
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth())
                .forPaths(PathSelectors.any()).build();
    }

    private List<SecurityReference> defaultAuth() {
        return Collections.singletonList(SecurityReference.builder()
                .reference("JWT")
                .scopes(new AuthorizationScope[0])
                .build());
    }


    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.epam.enrolleesystem.rest"))
                .paths(paths())
                .build()
                .consumes(Collections.singleton(MediaType.APPLICATION_JSON_VALUE))
                .produces(Collections.singleton(MediaType.APPLICATION_JSON_VALUE))
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, Lists.asList(contractMessage(HttpStatus.OK), DEFAULT_STATUSES))
                .globalResponseMessage(RequestMethod.PUT, Lists.asList(contractMessage(HttpStatus.OK), DEFAULT_STATUSES))
                .globalResponseMessage(RequestMethod.POST, Lists.asList(contractMessage(HttpStatus.CREATED), DEFAULT_STATUSES))
                .globalResponseMessage(RequestMethod.DELETE, Lists.asList(contractMessage(HttpStatus.NO_CONTENT), DEFAULT_STATUSES))
                .securitySchemes(Collections.singletonList(apiKey()))
                .securityContexts(Collections.singletonList(securityContext()));
    }

    @SuppressWarnings("Guava")
    private Predicate<String> paths() {
        return Predicates.and(
                PathSelectors.regex("/.*"),
                Predicates.not(PathSelectors.regex("/error.*"))
        );
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Enroll System APIs")
                .description("This page lists all the rest apis for Enroll System App.")
                .version("0.0.1-SNAPSHOT")
                .build();
    }

    private ApiKey apiKey() {
        return new ApiKey("Authorization", "Authorization", "header");
    }

    private static ResponseMessage contractMessage(HttpStatus status) {
        return new ResponseMessageBuilder()
                .code(status.getCode())
                .message(status.getMessage())
                .build();
    }
}
