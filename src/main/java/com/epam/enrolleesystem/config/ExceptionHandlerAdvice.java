package com.epam.enrolleesystem.config;

import com.epam.enrolleesystem.domain.ErrorResponse;
import com.epam.enrolleesystem.domain.HttpResponse;
import com.epam.enrolleesystem.exception.EnrollRuntimeException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler({EnrollRuntimeException.class})
    public ResponseEntity<HttpResponse<Object, ErrorResponse<Object>>> handleEnrollRuntimeException(EnrollRuntimeException ex) {
        ErrorResponse<Object> errorResponse = ErrorResponse.builder()
                .httpStatus(ex.getStatus())
                .currentDate(LocalDateTime.now())
                .message(ex.getMessage()).build();

        return HttpResponse.error(errorResponse);
    }


}
