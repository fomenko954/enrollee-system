package com.epam.enrolleesystem.config;

import com.epam.enrolleesystem.annotation.AuthenticatedUser;
import com.epam.enrolleesystem.domain.JwtUserDetails;
import com.epam.enrolleesystem.model.User;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.expression.BeanResolver;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.lang.annotation.Annotation;
import java.util.Objects;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class EnrolAuthenticationPrincipalArgumentResolver implements HandlerMethodArgumentResolver {

    ExpressionParser parser = new SpelExpressionParser();
    @Setter
    BeanResolver beanResolver;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return findMethodAnnotation(AuthenticatedUser.class, parameter) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(Objects.isNull(authentication)) {
            return null;
        }

        Object principal = authentication.getPrincipal();

        AuthenticatedUser authenticatedUser = findMethodAnnotation(AuthenticatedUser.class, parameter);

        String expressionToParse = Objects.requireNonNull(authenticatedUser)
                .expression();

        if(StringUtils.hasLength(expressionToParse)) {
            StandardEvaluationContext context = new StandardEvaluationContext();
            context.setRootObject(principal);
            context.setVariable("this", principal);
            context.setBeanResolver(beanResolver);

            Expression expression = this.parser.parseExpression(expressionToParse);
            principal = expression.getValue();
        }

        Class<?> parameterType = parameter.getParameterType();
        if(Objects.nonNull(principal) && !parameterType.isAssignableFrom(User.class)){
            if(authenticatedUser.errorOnInvalidType()) {
                throw new ClassCastException(principal + " is not assignable to "
                        + parameter.getParameterType());
            } else {
                return null;
            }
        }

        return Objects.nonNull(principal) &&
                principal.getClass().isAssignableFrom(JwtUserDetails.class) ?
                ((JwtUserDetails) principal).getUser() :
                principal;
    }

    private <T extends Annotation> T findMethodAnnotation(Class<T> annotationClass, MethodParameter parameter) {
        T annotation = parameter.getParameterAnnotation(annotationClass);
        if(Objects.nonNull(annotation)) {
            return annotation;
        }
        Annotation[] annotationsToSearch = parameter.getMethodAnnotations();
        for (Annotation searchedAnnotation : annotationsToSearch) {
            annotation = AnnotationUtils.findAnnotation(searchedAnnotation.annotationType(), annotationClass);
            if(Objects.nonNull(annotation)){
                return annotation;
            }
        }
        return null;
    }
}
