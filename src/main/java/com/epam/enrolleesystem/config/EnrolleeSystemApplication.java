package com.epam.enrolleesystem.config;

import com.epam.enrolleesystem.property.FileStorageProperty;
import com.epam.enrolleesystem.property.MessageTemplateProperty;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
@ComponentScan(basePackages = {"com.epam.enrolleesystem"})
@EntityScan({"com.epam.enrolleesystem.model"})
@EnableJpaRepositories({"com.epam.enrolleesystem.repository"})
@EnableTransactionManagement
@EnableConfigurationProperties({FileStorageProperty.class, MessageTemplateProperty.class})
@EnableScheduling
public class EnrolleeSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnrolleeSystemApplication.class, args);
	}

}
