package com.epam.enrolleesystem.config;

import com.epam.enrolleesystem.converter.FacultyDtoToFacultyConverter;
import com.epam.enrolleesystem.converter.RegistrationDtoToUserConverter;
import com.epam.enrolleesystem.converter.SubjectDtoToSubjectConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

@Configuration
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class WebConfig extends WebMvcConfigurationSupport{

    @Bean
    public RegistrationDtoToUserConverter registrationDtoToUserConverter(PasswordEncoder passwordEncoder) {
        return new RegistrationDtoToUserConverter(passwordEncoder);
    }

    @Bean
    public SubjectDtoToSubjectConverter subjectDtoToSubjectConverter() {
        return new SubjectDtoToSubjectConverter();
    }

    @Bean
    public FacultyDtoToFacultyConverter facultyDtoToFacultyConverter() {
        return new FacultyDtoToFacultyConverter();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public ConversionService defaultConversionService(PasswordEncoder passwordEncoder) {
        DefaultConversionService conversionService = new DefaultConversionService();
        conversionService.addConverter(registrationDtoToUserConverter(passwordEncoder));
        conversionService.addConverter(subjectDtoToSubjectConverter());
        conversionService.addConverter(facultyDtoToFacultyConverter());
        return conversionService;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Override
    protected void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new EnrolAuthenticationPrincipalArgumentResolver());
    }
}
