package com.epam.enrolleesystem.repository;

import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.model.usersubject.UserSubject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserSubjectRepository extends JpaRepository<UserSubject, Long> {
    List<UserSubject> findByUser(User user);
}
