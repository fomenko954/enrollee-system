package com.epam.enrolleesystem.repository;

import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.model.userfaculty.UserFaculty;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserFacultyRepository extends PagingAndSortingRepository<UserFaculty, Long>
{
    List<UserFaculty> findByUser(User user);
}
