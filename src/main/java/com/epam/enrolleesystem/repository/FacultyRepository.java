package com.epam.enrolleesystem.repository;

import com.epam.enrolleesystem.model.Faculty;
import com.epam.enrolleesystem.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FacultyRepository extends JpaRepository<Faculty, Long>
{

    List<Faculty> findDistinctBySubjectsIn(Iterable<Subject> subjects);


}
