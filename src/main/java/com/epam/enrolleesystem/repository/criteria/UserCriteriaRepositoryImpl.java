package com.epam.enrolleesystem.repository.criteria;

import com.epam.enrolleesystem.model.Role;
import com.epam.enrolleesystem.model.User;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class UserCriteriaRepositoryImpl implements UserCriteriaRepository{

    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String MIDDLE_NAME = "middleName";
    private static final String ROLE = "role";

    EntityManager entityManager;

    @Override
    public Page<User> searchUserByFullName(String fullName, Role role, Pageable pageable) {
        String requestFullName = constructRequestFullName(fullName);

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<User> root = query.from(User.class);

        Expression<String> flmFullName = criteriaBuilder.concat(root.get(FIRST_NAME), StringUtils.SPACE);
        flmFullName = criteriaBuilder.concat(flmFullName, root.get(LAST_NAME));
        flmFullName = criteriaBuilder.concat(flmFullName, " ");
        flmFullName = criteriaBuilder.concat(flmFullName, MIDDLE_NAME);

        Expression<String> lfmFullName = criteriaBuilder.concat(root.get(LAST_NAME), StringUtils.SPACE);
        flmFullName = criteriaBuilder.concat(flmFullName, root.get(FIRST_NAME));
        flmFullName = criteriaBuilder.concat(flmFullName, StringUtils.SPACE);
        flmFullName = criteriaBuilder.concat(flmFullName, MIDDLE_NAME);

        Predicate whereFullNameClause = criteriaBuilder.or(
                criteriaBuilder.like(flmFullName, requestFullName),
                criteriaBuilder.like(lfmFullName, requestFullName)
        );

        Predicate whereRoleClause = criteriaBuilder.and(
                criteriaBuilder.equal(root.get(ROLE), role)
        );

        query.select(root).where(whereFullNameClause, whereRoleClause);

        TypedQuery<User> userQuery = entityManager.createQuery(query);
        userQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        userQuery.setMaxResults(pageable.getPageSize());
        List<User> users = userQuery.getResultList();
        return new PageImpl<>(users, pageable, 0);

    }

    private String constructRequestFullName(String fullName) {
        return Optional.of(fullName)
                .map(fn -> StringUtils.wrap(fn, StringUtils.SPACE))
                .map(fn -> StringUtils.replace(fn, StringUtils.SPACE, "%"))
                .orElse(StringUtils.EMPTY);

    }
}
