package com.epam.enrolleesystem.repository.criteria;

import com.epam.enrolleesystem.model.Role;
import com.epam.enrolleesystem.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserCriteriaRepository {

    Page<User> searchUserByFullName(String fullName, Role role, Pageable pageable);
}
