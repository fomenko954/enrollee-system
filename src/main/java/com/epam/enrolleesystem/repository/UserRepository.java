package com.epam.enrolleesystem.repository;

import com.epam.enrolleesystem.model.Role;
import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.model.VerifyStatus;
import com.epam.enrolleesystem.repository.criteria.UserCriteriaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<User, Long>, UserCriteriaRepository
{
    Optional<User> findByEmail(String email);
    boolean existsByEmail(String email);
    Page<User> findAllByRole(Role role, Pageable pageRequest);
    Long countUsersByVerifyStatus(VerifyStatus verifyStatus);
}
