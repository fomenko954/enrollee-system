package com.epam.enrolleesystem.repository;

import com.epam.enrolleesystem.model.Subject;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SubjectRepository extends PagingAndSortingRepository<Subject, Long>
{
}
