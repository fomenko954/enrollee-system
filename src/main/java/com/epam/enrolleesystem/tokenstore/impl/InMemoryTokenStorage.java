package com.epam.enrolleesystem.tokenstore.impl;

import com.epam.enrolleesystem.model.AccessToken;
import com.epam.enrolleesystem.tokenstore.TokenStore;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component("inMemoryTokenStorage")
public class InMemoryTokenStorage implements TokenStore {

    private static final Map<String, AccessToken> TOKEN_STORE = new LinkedHashMap<>();

    @Override
    public void putToken(AccessToken accessToken) {
        TOKEN_STORE.put(accessToken.getUser().getEmail(), accessToken);
    }

    @Override
    public AccessToken getToken(String tokenId) {
        return TOKEN_STORE.get(tokenId);
    }

    @Override
    public void clean() {
        List<String> expiredTokenKeys = TOKEN_STORE.entrySet()
                .stream()
                .filter(entry -> entry.getValue().getExpiredDate().isBefore(LocalDateTime.now()))
                .map(Map.Entry::getKey).collect(Collectors.toList());

        expiredTokenKeys.forEach(TOKEN_STORE::remove);
    }
}
