package com.epam.enrolleesystem.tokenstore;

import com.epam.enrolleesystem.model.AccessToken;

public interface TokenStore {
    void putToken(AccessToken accessToken);
    AccessToken getToken(String email);
    void clean();
}
