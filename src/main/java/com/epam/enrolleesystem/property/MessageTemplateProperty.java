package com.epam.enrolleesystem.property;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "enrollee-system.email")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MessageTemplateProperty {
    String acceptEmailPath;
    String rejectEmailPath;
}
