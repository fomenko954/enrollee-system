package com.epam.enrolleesystem.rest;

import com.epam.enrolleesystem.domain.ErrorResponse;
import com.epam.enrolleesystem.domain.HttpResponse;
import com.epam.enrolleesystem.domain.RegistrationDto;
import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.service.UserService;
import com.epam.enrolleesystem.util.ApiUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Map;

import static com.epam.enrolleesystem.domain.TagConstants.GUEST_TAG;

@RestController
@CrossOrigin
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Api(value = "Registration Controller", tags = "Registration Controller")
public class RegistrationController {

    UserService userService;

    @PostMapping(value = "/registration", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiOperation(value = "Allows authorize user into system.", tags = {GUEST_TAG},
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<HttpResponse<User, ErrorResponse<Map<String, String>>>> registerNewUser(
            @Valid @ModelAttribute RegistrationDto registrationDto, @RequestParam(required = false) MultipartFile certificate,
            @RequestParam(required = false)  MultipartFile avatar, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            ErrorResponse<Map<String, String>> errorResponse = ApiUtils.mapToErrorResponse(bindingResult);
            return HttpResponse.error(errorResponse);
        }

        User user = userService.registerNewUser(registrationDto, avatar, certificate);

        return HttpResponse.created(user);
    }
}
