package com.epam.enrolleesystem.rest;

import com.epam.enrolleesystem.domain.CompliantDto;
import com.epam.enrolleesystem.domain.CompliantEnrollDto;
import com.epam.enrolleesystem.domain.ErrorResponse;
import com.epam.enrolleesystem.domain.HttpResponse;
import com.epam.enrolleesystem.domain.PageDto;
import com.epam.enrolleesystem.domain.ResponseWrapper;
import com.epam.enrolleesystem.model.userfaculty.UserFaculty;
import com.epam.enrolleesystem.service.ComplaintService;
import com.epam.enrolleesystem.service.UserService;
import com.epam.enrolleesystem.util.ApiUtils;
import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static com.epam.enrolleesystem.domain.TagConstants.ADMIN_TAG;
import static com.epam.enrolleesystem.domain.TagConstants.STUDENT_TAG;

@RestController
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequestMapping("/api/complaints")
@Api(value = "Complaint Resource", tags = "Complaint Controller")
public class ComplaintController {

    UserService userService;
    ComplaintService complaintService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @JsonView(View.CompliantResponseView.class)
    @ApiOperation(value = "Allows get paged list of the complaint", tags = ADMIN_TAG, produces = MediaType.APPLICATION_JSON_VALUE,
            authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<List<UserFaculty>, ErrorResponse<Map<String, String>>>> getAllComplaint(
            @Valid PageDto page, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            ErrorResponse<Map<String, String>> mapErrorResponse = ApiUtils.mapToErrorResponse(bindingResult);
            return HttpResponse.error(mapErrorResponse);
        }
        PageRequest pageable = PageRequest.of(page.getPage() - 1, page.getSize());
        return HttpResponse.okWithPage(complaintService.getAllCompliant(pageable));
    }


    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('STUDENT')")
    @JsonView(View.CompliantResponseView.class)
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Allows register user on specific faculty", tags = STUDENT_TAG, produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<ResponseWrapper<UserFaculty>, ErrorResponse<Map<String, String>>>> registerUserOnFaculty(
            @RequestBody @ApiParam(value = "Compliant", name = "Compliant") CompliantDto compliantDto) {
        return HttpResponse.created(userService.registerUserOnFaculty(compliantDto));
    }


    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @JsonView(View.CompliantResponseView.class)
    @ApiOperation(value = "Allows enroll or reject user on specific faculty", tags = ADMIN_TAG,
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE,
            authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<ResponseWrapper<UserFaculty>, ErrorResponse<Map<String, String>>>> enrollUserToFaculty(
            @RequestBody @ApiParam(value = "Compliant body", name = "Compliant")CompliantEnrollDto compliantDto) {
        return HttpResponse.ok(userService.enrollUserToFaculty(compliantDto));
    }
}
