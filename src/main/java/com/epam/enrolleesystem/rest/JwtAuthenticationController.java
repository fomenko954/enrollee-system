package com.epam.enrolleesystem.rest;

import com.epam.enrolleesystem.annotation.AuthenticatedUser;
import com.epam.enrolleesystem.domain.ErrorResponse;
import com.epam.enrolleesystem.domain.HttpResponse;
import com.epam.enrolleesystem.domain.JwtRequest;
import com.epam.enrolleesystem.domain.JwtResponse;
import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.service.AuthorizationService;
import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.epam.enrolleesystem.domain.TagConstants.ADMIN_TAG;
import static com.epam.enrolleesystem.domain.TagConstants.GUEST_TAG;
import static com.epam.enrolleesystem.domain.TagConstants.STUDENT_TAG;

@RestController
@CrossOrigin
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Api(value = "Authentication Controller", tags = "Authentication Controller")
public class JwtAuthenticationController {

    AuthorizationService authorizationService;

    @PostMapping("/authenticate")
    @JsonView(View.AuthenticationResponseView.class)
    @ApiOperation(value = "Allows authorize user into system.", tags = {GUEST_TAG},
            produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JwtResponse> createAuthenticationToken(@RequestBody @ApiParam(name = "Authorize request",
            value = "Input data for authorizing user.") JwtRequest jwtRequest) {

        return ResponseEntity.ok(authorizationService.authenticate(jwtRequest));
    }

    @GetMapping("/profile")
    @JsonView(View.ResponseView.class)
    @ApiOperation(value = "Allows get information about authorized user", tags = {ADMIN_TAG, STUDENT_TAG},
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<User, ErrorResponse<Map<String, String>>>> getProfile(@AuthenticatedUser User user) {
        return HttpResponse.ok(user);
    }
}
