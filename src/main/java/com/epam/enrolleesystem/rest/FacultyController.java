package com.epam.enrolleesystem.rest;

import com.epam.enrolleesystem.domain.ErrorResponse;
import com.epam.enrolleesystem.domain.FacultyDto;
import com.epam.enrolleesystem.domain.HttpResponse;
import com.epam.enrolleesystem.domain.PageDto;
import com.epam.enrolleesystem.model.Faculty;
import com.epam.enrolleesystem.service.FacultyService;
import com.epam.enrolleesystem.util.ApiUtils;
import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static com.epam.enrolleesystem.domain.TagConstants.ADMIN_TAG;
import static com.epam.enrolleesystem.domain.TagConstants.STUDENT_TAG;

@RestController
@RequestMapping(value = "/api/faculties", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Api(value = "Faculty Resource", tags = "Faculty Controller")
public class FacultyController {
    FacultyService facultyService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @JsonView(View.FacultySubjectResponseView.class)
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Allows create a new faculty.", tags = ADMIN_TAG, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<Faculty, ErrorResponse<Map<String, String>>>> addNewFaculty(@RequestBody
                                                                                                   @Valid FacultyDto facultyDto,
                                                                                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            ErrorResponse<Map<String, String>> mapErrorResponse = ApiUtils.mapToErrorResponse(bindingResult);
            return HttpResponse.error(mapErrorResponse);
        }
        return HttpResponse.created(facultyService.createNewFaculty(facultyDto));
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'STUDENT')")
    @JsonView(View.FacultySubjectResponseView.class)
    @ApiOperation(value = "Allows get paged list of the faculty.", tags = {ADMIN_TAG, STUDENT_TAG},
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<List<Faculty>, ErrorResponse<Map<String, String>>>> findAllFaculties(@Valid PageDto pageDto, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            ErrorResponse<Map<String, String>> error = ApiUtils.mapToErrorResponse(bindingResult);
            return HttpResponse.error(error);
        }
        PageRequest pageable = PageRequest.of(pageDto.getPage() - 1, pageDto.getSize());
        return HttpResponse.okWithPage(facultyService.findAllFaculties(pageable));
    }

    @DeleteMapping("/{facultyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ADMINISTRATOR')")
    @ApiOperation(value = "Allows delete specific faculty.", tags = ADMIN_TAG,
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<String, ErrorResponse<String>>> deleteFaculty(@PathVariable("facultyId")
                                                                                     @ApiParam(name = "Faculty id") Long facultyId) {
        facultyService.deleteFaculty(facultyId);
        return HttpResponse.successfullyDeleted();
    }
}
