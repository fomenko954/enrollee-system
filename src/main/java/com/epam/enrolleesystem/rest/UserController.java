package com.epam.enrolleesystem.rest;

import com.epam.enrolleesystem.annotation.AuthenticatedUser;
import com.epam.enrolleesystem.domain.ErrorResponse;
import com.epam.enrolleesystem.domain.HttpResponse;
import com.epam.enrolleesystem.domain.PageDto;
import com.epam.enrolleesystem.domain.ResponseWrapper;
import com.epam.enrolleesystem.domain.UserDto;
import com.epam.enrolleesystem.domain.UserSubjectList;
import com.epam.enrolleesystem.model.Faculty;
import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.model.VerifyStatus;
import com.epam.enrolleesystem.model.userfaculty.UserFaculty;
import com.epam.enrolleesystem.model.usersubject.UserSubject;
import com.epam.enrolleesystem.service.ComplaintService;
import com.epam.enrolleesystem.service.FacultyService;
import com.epam.enrolleesystem.service.UserService;
import com.epam.enrolleesystem.util.ApiUtils;
import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static com.epam.enrolleesystem.domain.TagConstants.ADMIN_TAG;
import static com.epam.enrolleesystem.domain.TagConstants.STUDENT_TAG;

@RestController
@RequestMapping(value = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Api(value = "User Resource", tags = "User Controller")
@CrossOrigin
public class UserController {
    UserService userService;
    FacultyService facultyService;
    ComplaintService complaintService;

    @PostMapping("/marks")
    @JsonView({View.MarkResponseView.class})
    @PreAuthorize("hasRole('STUDENT')")
    @ApiOperation(value = "Allows put marks for specified subject.", tags = {STUDENT_TAG}, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<ResponseWrapper<UserSubject>, ErrorResponse<Map<String, String>>>> putSubjectMarkForUser(@AuthenticatedUser @ApiIgnore User user,
                                                                                                                                @RequestBody @Valid UserSubjectList userSubjectDto,
                                                                                                                                BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            ErrorResponse<Map<String, String>> mapErrorResponse = ApiUtils.mapToErrorResponse(bindingResult);
            return HttpResponse.error(mapErrorResponse);
        }
        ResponseWrapper<UserSubject> userSubjects = userService.putMarkForSubjects(user, userSubjectDto);
        return HttpResponse.ok(userSubjects);
    }

    @GetMapping(value = "/complaints")
    @PreAuthorize("hasRole('STUDENT')")
    @JsonView(View.CompliantResponseView.class)
    @ApiOperation(value = "Allows get own list of the complaint.", tags = {STUDENT_TAG},
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<ResponseWrapper<UserFaculty>, ErrorResponse<Map<String, String>>>> getOwnCompliant(@AuthenticatedUser @ApiIgnore User user) {

        List<UserFaculty> complaints = complaintService.getComplaintsByUser(user);
        ResponseWrapper<UserFaculty> userFaculties = ResponseWrapper.<UserFaculty>builder()
                .user(user)
                .data(complaints)
                .build();
        return HttpResponse.ok(userFaculties);
    }

    @GetMapping(value = "/{userId}/complaints")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @JsonView(View.CompliantResponseView.class)
    @ApiOperation(value = "Allows get list of the complaint by user.", tags = {ADMIN_TAG},
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<ResponseWrapper<UserFaculty>, ErrorResponse<Map<String, String>>>> getCompliantByUser(@PathVariable("userId") Long userId) {
        User user = userService.getUserById(userId);
        List<UserFaculty> complaints = complaintService.getComplaintsByUser(user);
        ResponseWrapper<UserFaculty> userFaculties = ResponseWrapper.<UserFaculty>builder()
                .user(user)
                .data(complaints)
                .build();
        return HttpResponse.ok(userFaculties);
    }

    @GetMapping("/marks")
    @PreAuthorize("hasRole('STUDENT')")
    @JsonView(View.MarkResponseView.class)
    @ApiOperation(value = "Allows get own list of the marks.", tags = {STUDENT_TAG},
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<ResponseWrapper<UserSubject>, ErrorResponse<Map<String, String>>>> getOwnMarks(@AuthenticatedUser @ApiIgnore User user) {
        List<UserSubject> userSubjects = userService.getAllMarksByUser(user);
        ResponseWrapper<UserSubject> response = ResponseWrapper.<UserSubject>builder()
                .user(user)
                .data(userSubjects)
                .build();
        return HttpResponse.ok(response);
    }

    @GetMapping("/{userId}/marks")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @JsonView(View.MarkResponseView.class)
    @ApiOperation(value = "Allows get own list of the marks.", tags = {ADMIN_TAG},
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<ResponseWrapper<UserSubject>, ErrorResponse<Map<String, String>>>> getMarksByUser(@PathVariable("userId") Long userId) {
        User user = userService.getUserById(userId);
        List<UserSubject> userSubjects = userService.getAllMarksByUser(user);
        ResponseWrapper<UserSubject> response = ResponseWrapper.<UserSubject>builder()
                .user(user)
                .data(userSubjects)
                .build();
        return HttpResponse.ok(response);
    }

    @GetMapping("/faculties")
    @PreAuthorize("hasAnyRole('STUDENT')")
    @ApiOperation(value = "Allows get allowed faculty list for user.", tags = {STUDENT_TAG},
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    @JsonView(View.FacultySubjectResponseView.class)
    public ResponseEntity<HttpResponse<List<Faculty>, ErrorResponse<Map<String, String>>>> getAllowedFacultiesByUser(@AuthenticatedUser @ApiIgnore User user) {
        return HttpResponse.ok(facultyService.getAllowedFacultiesByUser(user));
    }

    @PutMapping("/{userId}/block")
    @JsonView(View.ResponseView.class)
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @ApiOperation(value = "Allows block user user.", tags = {ADMIN_TAG},
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<User, ErrorResponse<Map<String, String>>>> setBlockStatus(
            @PathVariable("userId") Long userId, @RequestParam("isBlock") boolean isBlock) {
        return HttpResponse.ok(userService.setBlockStatusToUser(userId, isBlock));
    }

    @PutMapping
    @JsonView({View.ResponseView.class})
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'STUDENT')")
    @ApiOperation(value = "Allows update default user information", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE, authorizations = @Authorization("Authorization"))
    public ResponseEntity<HttpResponse<User, ErrorResponse<Map<String, String>>>> updateUser(UserDto userDto, @AuthenticatedUser User user) {
        return HttpResponse.ok(userService.updateUserInformation(userDto, user));
    }

    @PutMapping("/avatar")
    @JsonView({View.ResponseView.class})
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'STUDENT')")
    @ApiOperation(value = "Allows update user's avatar", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE, authorizations = @Authorization("Authorization"))
    public ResponseEntity<HttpResponse<User, ErrorResponse<Map<String, String>>>> changeAvatar(@RequestParam("avatar") MultipartFile avatar,
                                                                                               @AuthenticatedUser User user) {
        return HttpResponse.ok(userService.changeAvatar(avatar, user));
    }

    @PutMapping("/{userId}/status")
    @JsonView(View.ResponseView.class)
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @ApiOperation(value = "Allows set verification status to user.", tags = {ADMIN_TAG}, produces = MediaType.APPLICATION_JSON_VALUE,
            authorizations = @Authorization("Authorization"))
    public ResponseEntity<HttpResponse<User, ErrorResponse<Map<String, String>>>> changeUserVerifiedStatus(
            @PathVariable("userId") Long userId,
            @RequestParam("verifyStatus")
            @ApiParam(name = "Status", required = true, allowableValues = "VERIFIED,REJECTED") VerifyStatus verifyStatus) {
        return HttpResponse.ok(userService.setVerifiedStatusToUser(userId, verifyStatus));
    }

    @GetMapping
    @JsonView(View.MarkResponseView.class)
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @ApiOperation(value = "Allows get list of students.", tags = {ADMIN_TAG}, produces = MediaType.APPLICATION_JSON_VALUE,
            authorizations = @Authorization("Authorization"))
    public ResponseEntity<HttpResponse<List<User>, ErrorResponse<Map<String, String>>>> getAllStudents(
            @Valid PageDto pageDto,
            @RequestParam(value = "fullNameQ", required = false)
            @ApiParam(value = "Using for searching user by full name", allowEmptyValue = true) String fullName,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            ErrorResponse<Map<String, String>> error = ApiUtils.mapToErrorResponse(bindingResult);
            return HttpResponse.error(error);
        }
        PageRequest pageRequest = PageRequest.of(pageDto.getPage() - 1, pageDto.getSize());
        return HttpResponse.okWithPage(userService.getAllStudents(fullName, pageRequest));
    }

    @GetMapping("/count")
    @JsonView(View.MarkResponseView.class)
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @ApiOperation(value = "Allows get list of students.", tags = {ADMIN_TAG}, produces = MediaType.APPLICATION_JSON_VALUE,
            authorizations = @Authorization("Authorization"))
    public ResponseEntity<HttpResponse<Long, ErrorResponse<Map<String, String>>>> getAllStudents() {
        return HttpResponse.ok(userService.getCountOfNewUsers());
    }

}
