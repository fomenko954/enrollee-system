package com.epam.enrolleesystem.rest;

import com.epam.enrolleesystem.domain.ErrorResponse;
import com.epam.enrolleesystem.domain.HttpResponse;
import com.epam.enrolleesystem.domain.PageDto;
import com.epam.enrolleesystem.domain.SubjectDto;
import com.epam.enrolleesystem.model.Subject;
import com.epam.enrolleesystem.service.SubjectService;
import com.epam.enrolleesystem.util.ApiUtils;
import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static com.epam.enrolleesystem.domain.TagConstants.ADMIN_TAG;
import static com.epam.enrolleesystem.domain.TagConstants.STUDENT_TAG;

@RequestMapping("/api/subjects")
@RestController
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Api(value = "Subject Resource", tags = "Subject Controller")
public class SubjectController
{
    SubjectService subjectService;

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @JsonView({View.ResponseView.class})
    @ApiOperation(value = "Allows create a new subject.", tags = ADMIN_TAG, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<Subject, ErrorResponse<Map<String,String>>>> addNewSubject(@RequestBody @Valid SubjectDto subjectDto, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            ErrorResponse<Map<String, String>> mapErrorResponse = ApiUtils.mapToErrorResponse(bindingResult);
            return HttpResponse.error(mapErrorResponse);
        }
        return HttpResponse.created(subjectService.createNewSubject(subjectDto));
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'STUDENT')")
    @JsonView({View.ResponseView.class})
    @ApiOperation(value = "Allows get list of the subject.", tags = {ADMIN_TAG, STUDENT_TAG},
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<List<Subject>, ErrorResponse<Map<String, String>>>> getSubjects(@Valid PageDto pageDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            ErrorResponse<Map<String, String>> error = ApiUtils.mapToErrorResponse(bindingResult);
            return HttpResponse.error(error);
        }
        PageRequest pageRequest = PageRequest.of(pageDto.getPage() - 1, pageDto.getSize());
        return HttpResponse.okWithPage(subjectService.getPagedSubjects(pageRequest));
    }

    @GetMapping("/all")
    @PreAuthorize("hasAnyRole('ADMINISTRATOR', 'STUDENT')")
    @JsonView({View.ResponseView.class})
    @ApiOperation(value = "Allows get list of the subject.", tags = {ADMIN_TAG, STUDENT_TAG},
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<List<Subject>, ErrorResponse<Map<String, String>>>> getAllSubjects() {
        return HttpResponse.ok(subjectService.getAllSubjects());
    }

    @DeleteMapping("/{subjectId}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @JsonView({View.ResponseView.class})
    @ApiOperation(value = "Allows delete the subject.", tags = ADMIN_TAG, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<String, ErrorResponse<Map<String, String>>>> deleteSubject(@PathVariable("subjectId") Long subjectId) {
        subjectService.deleteSubject(subjectId);
        return HttpResponse.successfullyDeleted();
    }

    @PutMapping(value = "/{subjectId}",consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @JsonView({View.ResponseView.class})
    @ApiOperation(value = "Allows create a new subject.", tags = ADMIN_TAG, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<HttpResponse<Subject, ErrorResponse<Map<String,String>>>> updateSubject(@PathVariable("subjectId") Long subjectId,
            @RequestBody @Valid SubjectDto subjectDto, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            ErrorResponse<Map<String, String>> mapErrorResponse = ApiUtils.mapToErrorResponse(bindingResult);
            return HttpResponse.error(mapErrorResponse);
        }
        return HttpResponse.created(subjectService.updateSubject(subjectId, subjectDto));
    }

}
