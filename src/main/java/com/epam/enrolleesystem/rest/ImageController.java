package com.epam.enrolleesystem.rest;

import com.epam.enrolleesystem.domain.ImageHandlerType;
import com.epam.enrolleesystem.image.ImageHandler;
import com.epam.enrolleesystem.image.ImageHandlerFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static com.epam.enrolleesystem.domain.TagConstants.ADMIN_TAG;
import static com.epam.enrolleesystem.domain.TagConstants.STUDENT_TAG;

@RestController
@RequestMapping("/images")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Api(value = "Image Resource", tags = "Image Controller")
public class ImageController {
    ImageHandlerFactory imageHandlerFactory;

    @GetMapping(value = "/certificate/{fileName:.+}")
    //@PreAuthorize("hasAnyRole('ADMINISTRATOR', 'STUDENT')")
    @ApiOperation(value = "Allows get user's certificate.", tags = {STUDENT_TAG, ADMIN_TAG},
            produces = MediaType.IMAGE_PNG_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<InputStreamResource> getCertificate(@PathVariable String fileName) throws IOException {
        ImageHandler imageHandler = imageHandlerFactory.getImageHandler(ImageHandlerType.CERTIFICATE);
        Resource resource = imageHandler.loadImage(fileName);

        return ResponseEntity.ok()
                .contentLength(resource.contentLength())
                .contentType(MediaType.IMAGE_PNG)
                .body(new InputStreamResource(resource.getInputStream()));
    }

    @GetMapping(value = "/avatar/{fileName:.+}")
    //@PreAuthorize("hasAnyRole('ADMINISTRATOR', 'STUDENT')")
    @ApiOperation(value = "Allows get user's avatar.", tags = {STUDENT_TAG, ADMIN_TAG},
            produces = MediaType.IMAGE_PNG_VALUE, authorizations = {@Authorization("Authorization")})
    public ResponseEntity<InputStreamResource> getAvatar(@PathVariable String fileName) throws IOException {
        ImageHandler imageHandler = imageHandlerFactory.getImageHandler(ImageHandlerType.AVATAR);
        Resource resource = imageHandler.loadImage(fileName);

        return ResponseEntity.ok()
                .contentLength(resource.contentLength())
                .contentType(MediaType.IMAGE_PNG)
                .body(new InputStreamResource(resource.getInputStream()));
    }

}
