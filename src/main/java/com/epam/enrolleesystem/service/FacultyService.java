package com.epam.enrolleesystem.service;

import com.epam.enrolleesystem.domain.FacultyDto;
import com.epam.enrolleesystem.exception.EnrollRuntimeException;
import com.epam.enrolleesystem.exception.EntityNotFoundException;
import com.epam.enrolleesystem.model.Faculty;
import com.epam.enrolleesystem.model.Subject;
import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.model.usersubject.UserSubject;
import com.epam.enrolleesystem.repository.FacultyRepository;
import com.epam.enrolleesystem.repository.SubjectRepository;
import com.epam.enrolleesystem.util.AssertUtils;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class FacultyService {
    FacultyRepository facultyRepository;
    SubjectRepository subjectRepository;
    ConversionService defaultConversionService;

    @Transactional
    public Faculty createNewFaculty(FacultyDto facultyDto) {
        Faculty faculty = defaultConversionService.convert(facultyDto, Faculty.class);
        AssertUtils.assertNotNull(faculty, "Faculty is null", EnrollRuntimeException::new);
        List<Subject> subjects = (List<Subject>) subjectRepository.findAllById(facultyDto.getSubjectIds());
        faculty.setSubjects(new LinkedHashSet<>(subjects));
        subjects.forEach(subject -> subject.getFaculties().add(faculty));
        return facultyRepository.save(faculty);
    }

    public Faculty save(Faculty faculty) {
        return facultyRepository.save(faculty);
    }

    public Page<Faculty> findAllFaculties(Pageable pageable) {
        return facultyRepository.findAll(pageable);
    }

    public void deleteFaculty(long facultyId) {
        facultyRepository.deleteById(facultyId);
    }

    public Faculty getFacultyById(Long id) {
        return facultyRepository.findById(id)
                .orElseThrow(FacultyService::throwFacultyNotFoundException);
    }

    public List<Faculty> getAllowedFacultiesByUser(User user) {
        List<Subject> subjects = user.getUserSubjects()
                .stream()
                .map(UserSubject::getSubject)
                .collect(Collectors.toList());
        return facultyRepository.findDistinctBySubjectsIn(subjects);
    }

    private static EntityNotFoundException throwFacultyNotFoundException() {
        return new EntityNotFoundException("The faculty was not found.");
    }
}
