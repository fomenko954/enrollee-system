package com.epam.enrolleesystem.service;

import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.model.userfaculty.UserFaculty;
import com.epam.enrolleesystem.model.usersubject.UserSubject;
import com.epam.enrolleesystem.repository.UserFacultyRepository;
import com.epam.enrolleesystem.repository.UserSubjectRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashSet;
import java.util.List;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class ComplaintService
{
    UserFacultyRepository complaintRepository;
    UserSubjectRepository markRepository;

    public Page<UserFaculty> getAllCompliant(Pageable pageable) {
        return complaintRepository.findAll(pageable);
    }

    @Transactional
    public List<UserFaculty> getComplaintsByUser(User user) {
        List<UserSubject> userSubjects = markRepository.findByUser(user);
        user.setUserSubjects(new LinkedHashSet<>(userSubjects));

        return complaintRepository.findByUser(user);
    }
}
