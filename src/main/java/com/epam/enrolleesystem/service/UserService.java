package com.epam.enrolleesystem.service;

import com.epam.enrolleesystem.domain.CompliantDto;
import com.epam.enrolleesystem.domain.CompliantEnrollDto;
import com.epam.enrolleesystem.domain.ImageHandlerType;
import com.epam.enrolleesystem.domain.RegistrationDto;
import com.epam.enrolleesystem.domain.ResponseWrapper;
import com.epam.enrolleesystem.domain.UserDto;
import com.epam.enrolleesystem.domain.UserSubjectList;
import com.epam.enrolleesystem.exception.EnrollRuntimeException;
import com.epam.enrolleesystem.exception.EntityNotFoundException;
import com.epam.enrolleesystem.exception.UserAlreadyExistsException;
import com.epam.enrolleesystem.image.ImageHandler;
import com.epam.enrolleesystem.image.ImageHandlerFactory;
import com.epam.enrolleesystem.model.Faculty;
import com.epam.enrolleesystem.model.Role;
import com.epam.enrolleesystem.model.Subject;
import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.model.VerifyStatus;
import com.epam.enrolleesystem.model.userfaculty.RegistrationStatus;
import com.epam.enrolleesystem.model.userfaculty.UserFaculty;
import com.epam.enrolleesystem.model.userfaculty.UserFacultyId;
import com.epam.enrolleesystem.model.usersubject.UserSubject;
import com.epam.enrolleesystem.model.usersubject.UserSubjectId;
import com.epam.enrolleesystem.repository.UserRepository;
import com.epam.enrolleesystem.repository.UserSubjectRepository;
import com.google.common.collect.Lists;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class UserService {
    UserRepository userRepository;
    SubjectService subjectService;
    FacultyService facultyService;
    ImageHandlerFactory imageHandlerFactory;
    ConversionService defaultConversionService;
    MailService mailService;
    UserSubjectRepository markRepository;

    public User getUserById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("The user was not found."));
    }

    public User registerNewUser(RegistrationDto registrationDto, MultipartFile avatar, MultipartFile certificate) {
        if (userRepository.existsByEmail(registrationDto.getEmail())) {
            throw new UserAlreadyExistsException(String.format("User %s already exists.", registrationDto.getEmail()));
        }

        User user = defaultConversionService.convert(registrationDto, User.class);

        Objects.requireNonNull(user).setRole(Role.ROLE_STUDENT);
        user.setCertificateUrl(uploadImage(certificate, ImageHandlerType.CERTIFICATE));
        user.setAvatarUrl(uploadImage(avatar, ImageHandlerType.AVATAR));

        User savedUser = userRepository.save(user);
        savedUser.setPassword(null);
        return savedUser;
    }

    @Transactional
    public ResponseWrapper<UserSubject> putMarkForSubjects(User user, UserSubjectList userSubjectDto) {
        Set<Subject> subjects = new HashSet<>();

        Set<UserSubject> marks = userSubjectDto.getMarks()
                .stream()
                .map(usd -> {
                    Subject subject = subjectService.getSubjectById(usd.getSubjectId());

                    subjects.add(subject);
                    return new UserSubject(new UserSubjectId(user.getId(), subject.getId()), user, subject, usd.getMark());
                }).collect(Collectors.toSet());

        user.setUserSubjects(marks);
        subjects.forEach(subject -> subject.setUserSubjects(marks));
        user.setVerifyStatus(VerifyStatus.NOT_VERIFIED);

        userRepository.save(user);
        subjectService.saveAll(subjects);

        return ResponseWrapper.<UserSubject>builder()
                .user(user)
                .data(Lists.newArrayList(marks))
                .build();
    }

    @Transactional
    public ResponseWrapper<UserFaculty> registerUserOnFaculty(CompliantDto compliantDto) {
        User user = getUserById(compliantDto.getUserId());

        if(VerifyStatus.NOT_VERIFIED.equals(user.getVerifyStatus())
                || VerifyStatus.REJECTED.equals(user.getVerifyStatus())) {
            throw new EnrollRuntimeException("User's marks aren't verified.");
        }

        Faculty faculty = facultyService.getFacultyById(compliantDto.getFacultyId());

        Set<UserSubject> userSubjects = user.getUserSubjects();
        Set<Subject> facultySubjects = faculty.getSubjects();
        boolean isSubjectEquals = userSubjects.
                stream().
                allMatch(us -> facultySubjects.contains(us.getSubject()));

        if (!isSubjectEquals) {
            throw new EnrollRuntimeException("The user cannot be registered on the specified faculty due to user subjects" +
                    "is not match with the faculty subjects.");
        }

        Integer summaryMark = user.getUserSubjects()
                .stream()
                .mapToInt(UserSubject::getMark)
                .sum();

        UserFaculty userFaculty = new UserFaculty(new UserFacultyId(user.getId(), faculty.getId()), user, faculty,
                RegistrationStatus.REGISTERED, calculateExpiredDate(), null, summaryMark);

        faculty.getUserFaculties().add(userFaculty);
        user.getUserFaculties().add(userFaculty);

        facultyService.save(faculty);

        User savedUser = userRepository.save(user);

        return ResponseWrapper.<UserFaculty>builder()
                .user(user)
                .data(Lists.newArrayList(savedUser.getUserFaculties()))
                .build();
    }



    @Transactional
    public ResponseWrapper<UserFaculty> enrollUserToFaculty(CompliantEnrollDto compliantDto) {

        User user = getUserById(compliantDto.getUserId());

        Faculty faculty = facultyService.getFacultyById(compliantDto.getFacultyId());

        user.setUserFaculties(enrollUser(user.getUserFaculties(), user.getId(), faculty.getId(), compliantDto.getStatus()));
        faculty.setUserFaculties(enrollUser(faculty.getUserFaculties(), user.getId(), faculty.getId(), compliantDto.getStatus()));

        userRepository.save(user);
        facultyService.save(faculty);

        mailService.sendAcceptOrRejectMessage(user.getEmail(), compliantDto.getStatus(), user.getFirstName(), faculty.getName());

        return ResponseWrapper.<UserFaculty>builder()
                .user(user)
                .data(Lists.newArrayList(user.getUserFaculties()))
                .build();
    }

    public List<UserSubject> getAllMarksByUser(User user) {
        return markRepository.findByUser(user);
    }

    public User setBlockStatusToUser(Long userId, boolean isBlock) {
        User user = getUserById(userId);
        boolean userIsBlocked = user.getIsBlocked();
        if (userIsBlocked && isBlock) {
            throw new EnrollRuntimeException(String.format("User with id: %d already blocked.", user.getId()));
        }

        user.setIsBlocked(isBlock);

        return userRepository.save(user);
    }

    public User setVerifiedStatusToUser(Long userId, VerifyStatus verifyStatus) {
        User user = getUserById(userId);
        if(VerifyStatus.NOT_VERIFIED.equals(verifyStatus)) {
            throw new EnrollRuntimeException("Cannot set 'NOT_VERIFIED' status");
        }

        user.setVerifyStatus(verifyStatus);
        return userRepository.save(user);
    }

    public User updateUserInformation(UserDto userDto, User user) {
        fillUser(user, userDto);

        return userRepository.save(user);
    }

    public User changeAvatar(MultipartFile avatar, User user) {
        if(Objects.nonNull(avatar)) {
            user.setAvatarUrl(uploadImage(avatar, ImageHandlerType.AVATAR));
        }

        return userRepository.save(user);
    }

    public Page<User> getAllStudents(String userName, PageRequest pageRequest) {
        Page<User> users;
        if(StringUtils.isBlank(userName)) {
            users = userRepository.findAllByRole(Role.ROLE_STUDENT, pageRequest);
        } else {
            users = userRepository.searchUserByFullName(userName, Role.ROLE_STUDENT, pageRequest);

        }
        return users;
    }

    public Long getCountOfNewUsers() {
        return userRepository.countUsersByVerifyStatus(VerifyStatus.NOT_VERIFIED);
    }

    private void fillUser(User user, UserDto userDto) {
        if(userDtoIsNotEmpty(userDto)) {
            user.setFirstName(userDto.getFirstName());
            user.setLastName(userDto.getLastName());
            user.setMiddleName(userDto.getMiddleName());
            user.setSchoolNumber(userDto.getSchoolNumber());
            user.setCity(userDto.getCity());
            user.setRegion(userDto.getRegion());
        }
    }

    private boolean userDtoIsNotEmpty(UserDto userDto) {
        return !StringUtils.isAllBlank(userDto.getFirstName(), userDto.getLastName(),
                userDto.getMiddleName(), userDto.getCity(), userDto.getRegion())
                && Objects.nonNull(userDto.getSchoolNumber());
    }

    private Set<UserFaculty> enrollUser(Set<UserFaculty> userFaculties, Long userId, Long facultyId, RegistrationStatus status) {
        return userFaculties
                .stream()
                .filter(userFaculty -> userId.equals(userFaculty.getUser().getId()) && facultyId.equals(userFaculty.getFaculty().getId()))
                .peek(userFaculty -> {
                    userFaculty.setAcceptedDate(new Date());
                    userFaculty.setStatus(status);
                }).collect(Collectors.toSet());
    }

    private String uploadImage(MultipartFile file, ImageHandlerType type) {
        ImageHandler imageHandler = imageHandlerFactory.getImageHandler(type);
        return imageHandler.uploadImage(file);
    }

    private Date calculateExpiredDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(Calendar.getInstance().getTimeInMillis()));
        calendar.add(Calendar.DAY_OF_MONTH, 14);
        return new Date(calendar.getTimeInMillis());
    }
}
