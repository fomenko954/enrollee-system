package com.epam.enrolleesystem.service;

import com.epam.enrolleesystem.domain.SubjectDto;
import com.epam.enrolleesystem.exception.EnrollRuntimeException;
import com.epam.enrolleesystem.exception.EntityNotFoundException;
import com.epam.enrolleesystem.model.Subject;
import com.epam.enrolleesystem.repository.SubjectRepository;
import com.epam.enrolleesystem.util.AssertUtils;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SubjectService {

    SubjectRepository subjectRepository;
    ConversionService defaultConversionService;

    public Subject createNewSubject(SubjectDto subjectDto) {
        Subject subject = defaultConversionService.convert(subjectDto, Subject.class);
        AssertUtils.assertNotNull(subject, "Subject is null.", EnrollRuntimeException::new);
        return subjectRepository.save(subject);
    }

    public List<Subject> saveAll(Collection<Subject> subjects) {
        return (List<Subject>) subjectRepository.saveAll(subjects);
    }

    public Subject save(Subject subject) {
        return subjectRepository.save(subject);
    }

    public Subject getSubjectById(Long subjectId) {
        return subjectRepository.findById(subjectId)
                .orElseThrow(SubjectService::throwSubjectNotFoundException);
    }

    public Page<Subject> getPagedSubjects(Pageable pageable) {
        return subjectRepository.findAll(pageable);
    }

    public List<Subject> getAllSubjectsById(List<Long> ids) {
        return Collections.unmodifiableList((List<Subject>) subjectRepository.findAllById(ids));
    }

    public void deleteSubject(Long subjectId) {
        Subject subjectById = getSubjectById(subjectId);
        subjectRepository.delete(subjectById);
    }

    public Subject updateSubject(Long subjectId, SubjectDto subjectDto) {
        Subject subjectById = getSubjectById(subjectId);
        subjectById.setName(subjectDto.getName());
        return subjectRepository.save(subjectById);
    }

    public List<Subject> getAllSubjects() {
        return  Collections.unmodifiableList((List<Subject>) subjectRepository.findAll());
    }

    private static EntityNotFoundException throwSubjectNotFoundException() {
        return new EntityNotFoundException("The subject was not found.");
    }
}
