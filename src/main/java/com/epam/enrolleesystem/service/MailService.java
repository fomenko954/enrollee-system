package com.epam.enrolleesystem.service;

import com.epam.enrolleesystem.model.userfaculty.RegistrationStatus;
import com.epam.enrolleesystem.property.MessageTemplateProperty;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.apache.commons.io.FileUtils;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class MailService
{
    static Map<RegistrationStatus, String> templates = new LinkedHashMap<>();

    JavaMailSender javaMailSender;
    MessageTemplateProperty templateProperty;

    @PostConstruct
    public void initTemplates() throws IOException, URISyntaxException {
        URI acceptPath = this.getClass().getResource(templateProperty.getAcceptEmailPath()).toURI();
        URI rejectPath = this.getClass().getResource(templateProperty.getRejectEmailPath()).toURI();

        templates.put(RegistrationStatus.ENROLLED, FileUtils.readFileToString(
                new File(acceptPath), StandardCharsets.UTF_8));
        templates.put(RegistrationStatus.REJECTED, FileUtils.readFileToString(
                new File(rejectPath), StandardCharsets.UTF_8));
    }

    public void sendMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        javaMailSender.send(message);
    }

    public void sendAcceptOrRejectMessage(String email, RegistrationStatus status, Object... names) {
        String template = templates.get(status);
        String subject = status.equals(RegistrationStatus.ENROLLED) ? "Congratulation." : "Enroll system notification.";
        sendMessage(email, subject, String.format(template, names));
    }
}
