package com.epam.enrolleesystem.service;

import com.epam.enrolleesystem.domain.JwtRequest;
import com.epam.enrolleesystem.domain.JwtResponse;
import com.epam.enrolleesystem.domain.JwtUserDetails;
import com.epam.enrolleesystem.exception.EnrollRuntimeException;
import com.epam.enrolleesystem.model.AccessToken;
import com.epam.enrolleesystem.tokenstore.TokenStore;
import com.epam.enrolleesystem.util.JwtTokenUtils;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AuthorizationService {
    AuthenticationManager authenticationManager;
    JwtTokenUtils jwtTokenUtils;
    JwtUserDetailService jwtUserDetailService;
    TokenStore tokenStore;

    public JwtResponse authenticate(JwtRequest request) {
        authenticate(request.getEmail(), request.getPassword());

        JwtUserDetails userDetails = (JwtUserDetails) jwtUserDetailService.loadUserByUsername(request.getEmail());

        AccessToken accessToken = tokenStore.getToken(request.getEmail());

        if(Objects.isNull(accessToken)) {
            String token = jwtTokenUtils.generateToken(userDetails);
            Date tokenExpirationDate = jwtTokenUtils.getExpirationDateFromToken(token);
            accessToken = new AccessToken()
                    .setToken(token)
                    .setTokenId(UUID.randomUUID().toString())
                    .setUser(userDetails.getUser())
                    .setExpiredDate(tokenExpirationDate
                            .toInstant()
                            .atZone(ZoneId.systemDefault())
                            .toLocalDateTime());
            tokenStore.putToken(accessToken);
        }

        return JwtResponse.builder()
                .token(accessToken.getToken())
                .user(accessToken.getUser())
                .build();

    }

    private void authenticate(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (DisabledException e) {
            throw new EnrollRuntimeException("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new EnrollRuntimeException("INVALID_CREDENTIALS", e);
        }
    }
}
