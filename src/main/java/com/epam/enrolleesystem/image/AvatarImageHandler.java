package com.epam.enrolleesystem.image;

import com.epam.enrolleesystem.property.FileStorageProperty;
import com.epam.enrolleesystem.util.FileUtils;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.nio.file.Paths;

@Component
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AvatarImageHandler extends BaseImageHandler {

    FileStorageProperty fileStorageProperty;
    Path avatarStorageLocation;

    @Autowired
    public AvatarImageHandler(FileStorageProperty fileStorageProperty) {
        this.fileStorageProperty = fileStorageProperty;
        avatarStorageLocation = Paths.get(fileStorageProperty.getAvatarPath())
                .toAbsolutePath()
                .normalize();
        FileUtils.createDirectory(avatarStorageLocation);
    }

    @Override
    public String uploadImage(MultipartFile file) {
        return uploadImage(file, avatarStorageLocation, "/images/avatar/");
    }

    @Override
    public Resource loadImage(String fileName) {
        return loadFileAsResource(fileName, avatarStorageLocation);
    }
}
