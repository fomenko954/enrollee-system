package com.epam.enrolleesystem.image;

import com.epam.enrolleesystem.domain.ImageHandlerType;
import com.epam.enrolleesystem.exception.EnrollRuntimeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Map;

@Component
public class ImageHandlerFactory {

    private final Map<ImageHandlerType, ImageHandler> context;

    @Autowired
    public ImageHandlerFactory(CertificateImageHandler certificateImageHandler,
                               AvatarImageHandler avatarImageHandler) {
        context = new EnumMap<>(ImageHandlerType.class);
        context.put(ImageHandlerType.CERTIFICATE, certificateImageHandler);
        context.put(ImageHandlerType.AVATAR, avatarImageHandler);
    }

    public ImageHandler getImageHandler(ImageHandlerType type) {
        if(!context.containsKey(type)) {
            throw new EnrollRuntimeException(String.format("Unknown image handler type: %s.",type));
        }
        return context.get(type);
    }
}
