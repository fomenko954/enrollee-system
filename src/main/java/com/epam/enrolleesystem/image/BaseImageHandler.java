package com.epam.enrolleesystem.image;

import com.epam.enrolleesystem.exception.EnrollRuntimeException;
import com.epam.enrolleesystem.exception.FileStorageException;
import com.epam.enrolleesystem.util.AssertUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

public abstract class BaseImageHandler implements ImageHandler {

    String uploadImage(MultipartFile file, Path storageLocation, String imagePath) {
        String certificateUri = StringUtils.EMPTY;
        if (Objects.nonNull(file)) {
            String fileName = storeFile(file, storageLocation);

            certificateUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path(imagePath)
                    .path(fileName)
                    .toUriString();
        }
        return certificateUri;
    }

    private String storeFile(MultipartFile file, Path storageLocation) {
        AssertUtils.assertNotNull(file.getOriginalFilename(), "Original filenames is null.", FileStorageException::new);
        String fileName = org.springframework.util.StringUtils.cleanPath(file.getOriginalFilename());

        try {
            if (fileName.contains("..")) {
                throw new FileStorageException("The file contains invalid path sequence: " + fileName);
            }

            Path targetLocation = storageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException e) {
            throw new FileStorageException("Cannot store the file: " + fileName + ". Please try again");
        }
    }

    Resource loadFileAsResource(String fileName, Path storageLocation) {
        try {
            Path filePath = storageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new EnrollRuntimeException("File not found " + fileName);
            }

        } catch (MalformedURLException e) {
            throw new EnrollRuntimeException("File not found " + fileName);
        }
    }
}
