package com.epam.enrolleesystem.image;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface ImageHandler {
    String uploadImage(MultipartFile file);
    Resource loadImage(String fileName);
}
