package com.epam.enrolleesystem.converter;

import org.springframework.core.convert.converter.Converter;

import com.epam.enrolleesystem.domain.FacultyDto;
import com.epam.enrolleesystem.model.Faculty;


public class FacultyDtoToFacultyConverter implements Converter<FacultyDto, Faculty>
{

    @Override
    public Faculty convert(FacultyDto source)
    {
        return new Faculty()
                .setName(source.getName())
                .setFundedPlaceCount(source.getFundedPlaceCount())
                .setAllPlaceCount(source.getAllPlaceCount());
    }
}
