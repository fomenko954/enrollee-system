package com.epam.enrolleesystem.converter;

import org.springframework.core.convert.converter.Converter;

import com.epam.enrolleesystem.domain.SubjectDto;
import com.epam.enrolleesystem.model.Subject;

public class SubjectDtoToSubjectConverter implements Converter<SubjectDto, Subject>
{
    @Override
    public Subject convert(SubjectDto source)
    {
        return new Subject()
                .setName(source.getName());
    }
}
