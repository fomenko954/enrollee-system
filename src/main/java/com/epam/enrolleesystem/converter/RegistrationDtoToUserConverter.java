package com.epam.enrolleesystem.converter;

import com.epam.enrolleesystem.domain.RegistrationDto;
import com.epam.enrolleesystem.model.User;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor
public class RegistrationDtoToUserConverter implements Converter<RegistrationDto, User> {

    PasswordEncoder passwordEncoder;

    @Override
    public User convert(RegistrationDto source) {
        return new User()
                .setEmail(source.getEmail())
                .setPassword(passwordEncoder.encode(source.getPassword()))
                .setFirstName(source.getFirstName())
                .setLastName(source.getLastName())
                .setMiddleName(source.getMiddleName())
                .setRegion(source.getRegion())
                .setCity(source.getCity())
                .setSchoolNumber(source.getSchoolNumber())
                .setIsBlocked(false);
    }
}
