package com.epam.enrolleesystem.domain;

import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@JsonView(View.AuthenticationResponseView.class)
public class JwtResponse {
    String token;
    User user;
}
