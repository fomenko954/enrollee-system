package com.epam.enrolleesystem.domain;


import com.epam.enrolleesystem.model.userfaculty.RegistrationStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("CompliantEnroll")
public class CompliantEnrollDto extends CompliantDto {
    @ApiModelProperty(required = true, example = "REJECTED,ENROLLED", allowableValues = "REGISTERED,REJECTED,ENROLLED")
    RegistrationStatus status;
}
