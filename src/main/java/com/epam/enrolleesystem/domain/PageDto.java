package com.epam.enrolleesystem.domain;

import io.swagger.annotations.ApiParam;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class PageDto {
    @ApiParam(value = "Size", defaultValue = "20", example = "20")
    @Min(1)
    @Max(100)
    Integer size = 20;
    @ApiParam(value = "Page", defaultValue = "1", example = "1")
    @Min(1)
    Integer page = 1;
}
