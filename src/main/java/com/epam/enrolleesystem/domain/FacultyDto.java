package com.epam.enrolleesystem.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FacultyDto
{

    @NotBlank
    String name;
    @Positive
    Integer fundedPlaceCount;
    @Positive
    Integer allPlaceCount;
    @Size(min = 3, max = 3, message = "The faculty cannot contains more or less subject than 3.")
    List<Long> subjectIds;
}
