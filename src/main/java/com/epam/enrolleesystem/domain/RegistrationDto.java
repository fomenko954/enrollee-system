package com.epam.enrolleesystem.domain;

import com.epam.enrolleesystem.constraint.FieldMatch;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(chain = true)
@NoArgsConstructor
@FieldMatch(firstField = "password", secondField = "confirmPassword", message = "The password fields must match")
public class RegistrationDto {

    @NotEmpty(message = "First name must not be empty.")
    String firstName;
    @NotEmpty(message = "Last name must not be empty.")
    String lastName;
    @NotEmpty(message = "Middle name must not be empty.")
    String middleName;
    @NotEmpty(message = "Region must not be empty.")
    String region;
    @NotEmpty(message = "City must not be empty.")
    String city;
    @NotNull(message = "School number must not be empty.")
    Integer schoolNumber;
    @Email(message = "Email should be valid.")
    String email;
    @NotEmpty(message = "Password must not be empty")
    String password;
    @NotEmpty(message = "Confirm password must not be empty")
    String confirmPassword;
}
