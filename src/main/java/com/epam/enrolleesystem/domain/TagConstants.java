package com.epam.enrolleesystem.domain;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TagConstants {
    public static final String STUDENT_TAG = "Student endpoints";
    public static final String ADMIN_TAG = "Admin endpoints";
    public static final String GUEST_TAG = "Guest endpoints";
}
