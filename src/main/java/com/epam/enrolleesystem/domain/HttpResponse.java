package com.epam.enrolleesystem.domain;

import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(chain = true)
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonView({View.ResponseView.class, View.UserFacultyResponseView.class, View.CompliantResponseView.class, View.MarkResponseView.class})
public class HttpResponse<C, E>
{
    C content;
    E error;
    long currentPage;
    long totalPage;
    long size;

    public static <C, E> HttpResponse<C, E> content(C content) {
        return new HttpResponse<C, E>()
                .setContent(content);
    }


    public static <C, E> ResponseEntity<HttpResponse<C, E>> ok(C httpResponse) {
        HttpResponse<C, E> response = new HttpResponse<C, E>()
                .setContent(httpResponse);

        return ResponseEntity.ok(response);
    }

    @SuppressWarnings("unchecked")
    public static <C, E> ResponseEntity<HttpResponse<C, E>> okWithPage(Page<?> httpResponse) {
        HttpResponse<C, E> response = new HttpResponse<C, E>()
                .setContent(((C) Collections.unmodifiableList(httpResponse.getContent())))
                .setCurrentPage(httpResponse.getNumber() + 1)
                .setSize(httpResponse.getSize())
                .setTotalPage(httpResponse.getTotalPages());

        return ResponseEntity.ok(response);
    }

    public static <C, E> ResponseEntity<HttpResponse<C,E>> error(E error) {
        HttpResponse<C, E> response = new HttpResponse<C, E>()
                .setError(error);

        return ResponseEntity
                .status(((ErrorResponse<?>) error).getHttpStatus())
                .body(response);
    }

    public static <C, E> ResponseEntity<HttpResponse<C, E>> created(C content) {
        HttpResponse<C, E> response = new HttpResponse<C, E>()
                .setContent(content);

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    public static <E> ResponseEntity<HttpResponse<String, E>> successfullyDeleted() {
        HttpResponse<String, E> response = new HttpResponse<String, E>()
                .setContent("Ok");

        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(response);
    }
}
