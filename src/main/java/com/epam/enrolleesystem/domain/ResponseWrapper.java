package com.epam.enrolleesystem.domain;

import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@EqualsAndHashCode
@Getter
public class ResponseWrapper<T> {
    @JsonView({View.MarkResponseView.class, View.CompliantResponseView.class})
    User user;
    @JsonView({View.MarkResponseView.class, View.CompliantResponseView.class})
    List<T> data;
}
