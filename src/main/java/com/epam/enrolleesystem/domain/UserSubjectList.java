package com.epam.enrolleesystem.domain;

import javax.validation.constraints.Size;
import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class UserSubjectList
{
    @Size(min = 3, max = 3, message = "Required subjects size for user is `3`.")
    List<UserSubjectDto> marks;
}
