package com.epam.enrolleesystem.domain;

public enum ImageHandlerType {
    CERTIFICATE, AVATAR
}
