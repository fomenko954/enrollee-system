package com.epam.enrolleesystem.constraint;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FieldMatchValidator implements ConstraintValidator<FieldMatch, Object> {

    String firstFieldName;
    String secondFieldName;
    String message;

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        boolean isValid = true;
        try {
            Object first = BeanUtils.getProperty(value, firstFieldName);
            Object second = BeanUtils.getProperty(value, secondFieldName);

            isValid = Objects.isNull(first) && Objects.isNull(second) || Objects.nonNull(first) && first.equals(second);
        } catch (Exception e) {
            log.error("Something went wrong", e);
        }

        if (!isValid) {
            context.buildConstraintViolationWithTemplate(message)
                    .addPropertyNode(firstFieldName)
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();
        }

        return isValid;
    }

    @Override
    public void initialize(FieldMatch constraintAnnotation) {
        firstFieldName = constraintAnnotation.firstField();
        secondFieldName = constraintAnnotation.secondField();
        message = constraintAnnotation.message();
    }
}
