package com.epam.enrolleesystem.model;

import com.epam.enrolleesystem.model.userfaculty.UserFaculty;
import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "faculties")
@Data
@EqualsAndHashCode(callSuper = true, exclude = {"subjects", "userFaculties"})
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString(exclude = {"subjects", "userFaculties"})
public class Faculty extends Model {
    @JsonView({View.ResponseView.class})
    String name;
    @Column(name = "funded_place_count")
    @JsonView({View.ResponseView.class})
    Integer fundedPlaceCount;
    @Column(name = "all_place_count")
    @JsonView({View.ResponseView.class})
    Integer allPlaceCount;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "faculty_subject",
            joinColumns = @JoinColumn(name = "faculty_id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id"))
    @JsonView(View.FacultySubjectResponseView.class)
    Set<Subject> subjects = new HashSet<>();
    @OneToMany(mappedBy = "faculty",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JsonView(View.FacultyUserResponseView.class)
    Set<UserFaculty> userFaculties = new HashSet<>();

    public void setUserFaculties(Set<UserFaculty> userFaculties) {
        this.userFaculties.clear();
        this.userFaculties.addAll(userFaculties);
    }
}
