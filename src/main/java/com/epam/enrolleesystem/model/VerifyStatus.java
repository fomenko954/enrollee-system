package com.epam.enrolleesystem.model;

public enum VerifyStatus {
    NOT_VERIFIED, VERIFIED, REJECTED
}
