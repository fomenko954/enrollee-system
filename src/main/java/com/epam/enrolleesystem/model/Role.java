package com.epam.enrolleesystem.model;

public enum Role {
    ROLE_ADMINISTRATOR, ROLE_STUDENT, ROLE_GUEST
}
