package com.epam.enrolleesystem.model;

import com.epam.enrolleesystem.model.userfaculty.UserFaculty;
import com.epam.enrolleesystem.model.usersubject.UserSubject;
import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(callSuper = true, exclude = {"userSubjects", "userFaculties"})
@Table(name = "users", indexes = @Index(name = "fullNameIndex", columnList = "first_name,last_name,middle_name"))
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@ToString(exclude = {"userSubjects", "userFaculties"})
public class User extends Model {
    @Column(name = "first_name", length = 64)
    @JsonView({View.ResponseView.class, View.AuthenticationResponseView.class, View.UserFacultyResponseView.class,
            View.FacultyUserResponseView.class, View.MarkResponseView.class})
    String firstName;
    @Column(name = "last_name", length = 64)
    @JsonView({View.ResponseView.class, View.AuthenticationResponseView.class, View.UserFacultyResponseView.class,
            View.FacultyUserResponseView.class})
    String lastName;
    @Column(name = "middle_name", length = 64)
    @JsonView({View.ResponseView.class, View.UserFacultyResponseView.class, View.FacultyUserResponseView.class})
    String middleName;
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    @JsonView({View.ResponseView.class, View.AuthenticationResponseView.class, View.UserFacultyResponseView.class,
            View.FacultyUserResponseView.class})
    Role role;
    @Column(name = "region")
    @JsonView({View.ResponseView.class})
    String region;
    @Column(name = "city")
    @JsonView({View.ResponseView.class})
    String city;
    @Column(name = "school_number")
    @JsonView({View.ResponseView.class})
    Integer schoolNumber;
    @Column(name = "certificate_url")
    @JsonView({View.ResponseView.class})
    String certificateUrl;
    @Column(name = "email")
    @JsonView({View.ResponseView.class, View.AuthenticationResponseView.class, View.UserFacultyResponseView.class,
            View.FacultyUserResponseView.class})
    String email;
    @Column(name = "password")
    @JsonIgnore
    String password;
    @Column(name = "is_blocked")
    @JsonView({View.ResponseView.class})
    Boolean isBlocked;
    @Column(name = "avatar_url")
    @JsonView(View.ResponseView.class)
    String avatarUrl;
    @Column(name = "verified_status")
    @JsonView(View.ResponseView.class)
    @Enumerated(EnumType.STRING)
    VerifyStatus verifyStatus;
    @OneToMany(mappedBy = "user",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @JsonView({View.UserFacultyResponseView.class, View.CompliantResponseView.class, View.MarkResponseView.class})
    Set<UserSubject> userSubjects = new LinkedHashSet<>();
    @OneToMany(mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    Set<UserFaculty> userFaculties = new HashSet<>();


    public void setUserSubjects(Set<UserSubject> userSubjects) {
        this.userSubjects.clear();
        this.userSubjects.addAll(userSubjects);
    }

    public void setUserFaculties(Set<UserFaculty> userFacultiesSet) {
        this.userFaculties.clear();
        this.userFaculties.addAll(userFacultiesSet);
    }
}


