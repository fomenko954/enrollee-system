package com.epam.enrolleesystem.model.usersubject;

import com.epam.enrolleesystem.model.Subject;
import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity
@Table(name = "user_subject")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSubject
{
    @EmbeddedId
    UserSubjectId id;
    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    @JsonView({View.UserSubjectResponseView.class})
    User user;

    @ManyToOne
    @MapsId("subjectId")
    @JoinColumn(name = "subject_id")
    @JsonView({View.UserSubjectResponseView.class, View.CompliantResponseView.class, View.MarkResponseView.class})
    Subject subject;
    @JsonView({View.UserSubjectResponseView.class, View.CompliantResponseView.class, View.MarkResponseView.class})
    Integer mark;
}
