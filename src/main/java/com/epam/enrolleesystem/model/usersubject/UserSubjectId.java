package com.epam.enrolleesystem.model.usersubject;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSubjectId implements Serializable {
    @Column(name = "user_id")
    Long userId;
    @Column(name = "subject_id")
    Long subjectId;
}
