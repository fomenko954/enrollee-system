package com.epam.enrolleesystem.model.userfaculty;

public enum RegistrationStatus
{
    REGISTERED, REJECTED, ENROLLED
}
