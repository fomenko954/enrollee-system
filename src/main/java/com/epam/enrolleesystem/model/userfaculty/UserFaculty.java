package com.epam.enrolleesystem.model.userfaculty;

import com.epam.enrolleesystem.model.Faculty;
import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "user_faculty")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"status", "acceptedDate", "expiredDate"})
public class UserFaculty
{
    @EmbeddedId
    UserFacultyId id;
    @ManyToOne
    @MapsId("userId")
    @JsonView({View.UserFacultyResponseView.class, View.CompliantResponseView.class})
    User user;
    @ManyToOne
    @MapsId("facultyId")
    @JsonView({View.ResponseView.class, View.CompliantResponseView.class})
    Faculty faculty;
    @Enumerated(EnumType.STRING)
    @JsonView({View.ResponseView.class, View.CompliantResponseView.class})
    RegistrationStatus status;
    @Column(name = "expired_date")
    @JsonView({View.ResponseView.class, View.CompliantResponseView.class})
    Date expiredDate;
    @Column(name = "accepted_date")
    @JsonView({View.ResponseView.class, View.CompliantResponseView.class})
    Date acceptedDate;
    @Column(name = "summary_marks")
    @JsonView({View.ResponseView.class, View.CompliantResponseView.class})
    Integer summaryMarks;
}
