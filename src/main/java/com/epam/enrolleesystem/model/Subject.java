package com.epam.enrolleesystem.model;

import com.epam.enrolleesystem.model.usersubject.UserSubject;
import com.epam.enrolleesystem.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "subjects")
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true, exclude = "userSubjects")
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString(exclude = {"userSubjects", "faculties"})
public class Subject extends Model
{

    @JsonView({View.ResponseView.class, View.FacultySubjectResponseView.class, View.CompliantResponseView.class})
    String name;
    @OneToMany(mappedBy = "subject",
            cascade = {CascadeType.PERSIST},
            fetch = FetchType.EAGER)
    Set<UserSubject> userSubjects = new LinkedHashSet<>();

    @ManyToMany(mappedBy = "subjects")
    @JsonView(View.SubjectFacultyResponseView.class)
    Set<Faculty> faculties = new HashSet<>();

    public void setUserSubjects(Set<UserSubject> userSubjects)
    {
        this.userSubjects.clear();
        this.userSubjects.addAll(userSubjects);
    }
}
