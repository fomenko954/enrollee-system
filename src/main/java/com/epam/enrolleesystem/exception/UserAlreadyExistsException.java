package com.epam.enrolleesystem.exception;

import org.springframework.http.HttpStatus;

public class UserAlreadyExistsException extends EnrollRuntimeException {
    public UserAlreadyExistsException(String format) {
        super(format, HttpStatus.BAD_REQUEST);
    }
}
