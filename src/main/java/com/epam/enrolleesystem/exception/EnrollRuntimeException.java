package com.epam.enrolleesystem.exception;

import org.springframework.http.HttpStatus;

public class EnrollRuntimeException extends RuntimeException {

    private HttpStatus status = HttpStatus.BAD_REQUEST;

    public EnrollRuntimeException() {
        super();
    }

    public EnrollRuntimeException(HttpStatus status) {
        this.status = status;
    }

    public EnrollRuntimeException(String message) {
        super(message);
    }

    public EnrollRuntimeException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public EnrollRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpStatus getStatus() {
        return status;
    }
}
