package com.epam.enrolleesystem.exception;

import org.springframework.http.HttpStatus;

public class EntityNotFoundException extends EnrollRuntimeException
{
    public EntityNotFoundException()
    {
        super("Entity was not found.", HttpStatus.NOT_FOUND);
    }

    public EntityNotFoundException(String message)
    {
        super(message, HttpStatus.NOT_FOUND);
    }
}
