package com.epam.enrolleesystem.exception;

import org.springframework.http.HttpStatus;

public class FileStorageException extends EnrollRuntimeException {
    public FileStorageException(String s) {
        super(s, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
