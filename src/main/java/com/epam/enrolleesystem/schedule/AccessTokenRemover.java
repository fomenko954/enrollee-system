package com.epam.enrolleesystem.schedule;

import com.epam.enrolleesystem.tokenstore.TokenStore;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AccessTokenRemover {
    private final TokenStore tokenStore;

    @Scheduled(cron = "${enrollee-system.remover.cron}")
    public void cleanStore() {
        tokenStore.clean();
    }
}
