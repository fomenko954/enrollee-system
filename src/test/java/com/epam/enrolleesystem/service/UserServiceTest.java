package com.epam.enrolleesystem.service;

import com.epam.enrolleesystem.converter.RegistrationDtoToUserConverter;
import com.epam.enrolleesystem.domain.CompliantDto;
import com.epam.enrolleesystem.domain.RegistrationDto;
import com.epam.enrolleesystem.domain.ResponseWrapper;
import com.epam.enrolleesystem.domain.UserSubjectList;
import com.epam.enrolleesystem.exception.EnrollRuntimeException;
import com.epam.enrolleesystem.exception.EntityNotFoundException;
import com.epam.enrolleesystem.exception.UserAlreadyExistsException;
import com.epam.enrolleesystem.image.AvatarImageHandler;
import com.epam.enrolleesystem.image.CertificateImageHandler;
import com.epam.enrolleesystem.image.ImageHandlerFactory;
import com.epam.enrolleesystem.model.Faculty;
import com.epam.enrolleesystem.model.Role;
import com.epam.enrolleesystem.model.Subject;
import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.model.userfaculty.UserFaculty;
import com.epam.enrolleesystem.model.usersubject.UserSubject;
import com.epam.enrolleesystem.repository.UserRepository;
import com.epam.enrolleesystem.repository.UserSubjectRepository;
import com.epam.enrolleesystem.utils.FacultyTestUtils;
import com.epam.enrolleesystem.utils.SubjectTestUtils;
import com.epam.enrolleesystem.utils.UserTestUtils;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    private final User user = UserTestUtils.createUserModel();
    private final RegistrationDto registrationDto = UserTestUtils.createUserDto();
    private final Faculty faculty = FacultyTestUtils.createFacultyModel();

    @Mock
    private UserRepository mockUserRepository;
    @Mock
    private CertificateImageHandler certificateHandler;
    @Mock
    private AvatarImageHandler avatarHandler;
    @Mock
    private SubjectService mockSubjectService;
    @Mock
    private FacultyService mockFacultyService;
    @Mock
    private MailService mockMailService;
    @Mock
    private UserSubjectRepository mockMarkRepository;

    private UserService userService;

    @BeforeEach
    public void setUp() {
        ImageHandlerFactory imageHandlerFactory = new ImageHandlerFactory(certificateHandler, avatarHandler);
        DefaultConversionService defaultConversionService = new DefaultConversionService();
        defaultConversionService.addConverter(new RegistrationDtoToUserConverter(Mockito.mock(PasswordEncoder.class)));
        userService = new UserService(mockUserRepository, mockSubjectService, mockFacultyService, imageHandlerFactory,
                defaultConversionService, mockMailService, mockMarkRepository);
    }

    @Test
    public void getUserById_ShouldReturnCorrectUser() {
        Mockito.when(mockUserRepository.findById(anyLong())).thenReturn(Optional.of(user));

        User actualUser = userService.getUserById(1L);

        assertEquals(user, actualUser, "User is not equals with expect user");
        Mockito.verify(mockUserRepository).findById(anyLong());
    }

    @Test
    public void getUserById_IfUserNotFound_ShouldThrowException() {
        Mockito.when(mockUserRepository.findById(anyLong())).thenReturn(Optional.empty());

        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class, () -> userService.getUserById(1L));
        String exMessage = exception.getMessage();
        HttpStatus expectedStatus = exception.getStatus();

        assertEquals("The user was not found.", exMessage);
        assertEquals(HttpStatus.NOT_FOUND, expectedStatus);
        Mockito.verify(mockUserRepository).findById(anyLong());
    }


    @Test
    public void createNewUser_ShouldCreateUser() {
        Mockito.when(mockUserRepository.save(any(User.class))).thenReturn(user);
        Mockito.when(certificateHandler.uploadImage(any(MultipartFile.class))).thenReturn("testCertificate");
        Mockito.when(avatarHandler.uploadImage(any(MultipartFile.class))).thenReturn("testAvatar");

        User user = userService.registerNewUser(registrationDto, avatar, certificate);

        assertUser(registrationDto, user);
        Mockito.verify(mockUserRepository).save(any(User.class));
        Mockito.verify(certificateHandler).uploadImage(any(MultipartFile.class));
        Mockito.verify(avatarHandler).uploadImage(any(MultipartFile.class));
    }

    @Test
    public void createNewUser_IfUserAlreadyExists_ShouldThrowException() {
        Mockito.when(mockUserRepository.existsByEmail(anyString())).thenReturn(true);

        UserAlreadyExistsException exception = assertThrows(UserAlreadyExistsException.class,
                () -> userService.registerNewUser(registrationDto, avatar, certificate));
        String message = exception.getMessage();
        HttpStatus status = exception.getStatus();

        assertEquals("User test@email.com already exists.", message);
        assertEquals(HttpStatus.BAD_REQUEST, status);
        Mockito.verify(mockUserRepository).existsByEmail(anyString());
    }

    @Test
    public void putMarkForSubjects_ShouldCorrectAddMark() {
        List<Subject> subjects = SubjectTestUtils.createSubjectList();
        UserSubjectList userSubjectList = UserTestUtils.createUserSubjectListDto();
        Mockito.when(mockSubjectService.getSubjectById(anyLong()))
                .thenReturn(subjects.get(0))
                .thenReturn(subjects.get(1))
                .thenReturn(subjects.get(2));
        Mockito.when(mockUserRepository.save(any(User.class))).thenReturn(user);
        Mockito.when(mockSubjectService.saveAll(anyCollection())).thenReturn(subjects);

        ResponseWrapper<UserSubject> actualMarks = userService.putMarkForSubjects(user, userSubjectList);
        ResponseWrapper<UserSubject> expectedMark = UserTestUtils.createUserSubjectsResponseWrapper();

        assertSet(expectedMark.getData(), actualMarks.getData(), userSubject -> userSubject.getId().hashCode());
        Mockito.verify(mockUserRepository).save(any(User.class));
        Mockito.verify(mockSubjectService).saveAll(anyCollection());
        Mockito.verify(mockSubjectService, Mockito.times(3)).getSubjectById(anyLong());
    }


    @Test
    public void registerUserOnFaculty_shouldCorrectRegister() {
        user.setUserSubjects(UserTestUtils.createUserSubjects());
        Mockito.when(mockUserRepository.findById(anyLong())).thenReturn(Optional.of(user));
        Mockito.when(mockFacultyService.getFacultyById(anyLong())).thenReturn(faculty);
        Mockito.when(mockFacultyService.save(any(Faculty.class))).thenReturn(faculty);
        Mockito.when(mockUserRepository.save(any(User.class))).thenReturn(user);

        CompliantDto compliantDto = UserTestUtils.createCompliantDto();
        ResponseWrapper<UserFaculty> expectedUserFaculties = UserTestUtils.createUserFacultiesResponseWrapper();
        ResponseWrapper<UserFaculty> actualUserFaculties = userService.registerUserOnFaculty(compliantDto);

        assertSet(expectedUserFaculties.getData(), actualUserFaculties.getData(), userFaculty -> userFaculty.getId().hashCode());
        Mockito.verify(mockUserRepository).findById(anyLong());
        Mockito.verify(mockUserRepository).save(any(User.class));
        Mockito.verify(mockFacultyService).getFacultyById(anyLong());
        Mockito.verify(mockFacultyService).save(any(Faculty.class));

    }

    @Test
    public void registerUserOnFaculty_ifUserNotExists_ShouldThrowException() {
        Mockito.when(mockUserRepository.findById(anyLong())).thenReturn(Optional.empty());

        CompliantDto compliantDto = UserTestUtils.createCompliantDto();
        EntityNotFoundException exception = assertThrows(EntityNotFoundException.class,
                () -> userService.registerUserOnFaculty(compliantDto));

        assertEquals("The user was not found.", exception.getMessage());
        assertEquals(HttpStatus.NOT_FOUND, exception.getStatus());
        Mockito.verify(mockUserRepository).findById(anyLong());
    }

    @Test
    public void registerUserOnFaculty_ifFUserSubjectsIsNotMatchWithFaculty_ShouldThrowException() {
        List<Subject> subjects = SubjectTestUtils.createSubjectList();
        subjects.set(1, SubjectTestUtils.createSubjectModel().setName("subTest5"));
        Set<UserSubject> userSubjects = UserTestUtils.createUserSubjects(subjects);
        user.setUserSubjects(userSubjects);
        Mockito.when(mockUserRepository.findById(anyLong())).thenReturn(Optional.of(user));
        Mockito.when(mockFacultyService.getFacultyById(anyLong())).thenReturn(faculty);

        CompliantDto compliantDto = UserTestUtils.createCompliantDto();
        EnrollRuntimeException exception = assertThrows(EnrollRuntimeException.class,
                () -> userService.registerUserOnFaculty(compliantDto));

        assertEquals("The user cannot be registered on the specified faculty due to user subjects" +
                "is not match with the faculty subjects.", exception.getMessage());
        assertEquals(HttpStatus.BAD_REQUEST, exception.getStatus());
        Mockito.verify(mockUserRepository).findById(anyLong());
        Mockito.verify(mockFacultyService).getFacultyById(anyLong());
    }

    @Test
    public void getAllMarkByUser_ShouldReturnCorrectMarks() {
        List<UserSubject> userSubjects = Lists.newArrayList(UserTestUtils.createUserSubjects());
        ResponseWrapper<UserSubject> expectedResponseWrapper = ResponseWrapper.<UserSubject>builder()
                .user(user)
                .data(userSubjects).build();
        Mockito.when(mockMarkRepository.findByUser(any(User.class))).thenReturn(userSubjects);

        ResponseWrapper<UserSubject> actualResponseWrapper = userService.getAllMarksByUser(user);

        assertEquals(expectedResponseWrapper, actualResponseWrapper);
        Mockito.verify(mockMarkRepository).findByUser(any(User.class));
    }

    private void assertUser(RegistrationDto registrationDto, User user) {
        assertEquals(registrationDto.getEmail(), user.getEmail());
        assertEquals(registrationDto.getFirstName(), user.getFirstName());
        assertEquals(registrationDto.getLastName(), user.getLastName());
        assertEquals(registrationDto.getMiddleName(), user.getMiddleName());
        assertEquals(registrationDto.getRegion(), user.getRegion());
        assertEquals(registrationDto.getCity(), user.getCity());
        assertEquals(registrationDto.getSchoolNumber(), user.getSchoolNumber());
        assertEquals(Role.ROLE_STUDENT, user.getRole());
        assertFalse(user.getIsBlocked());
    }

    private <T> void assertSet(List<T> expected, List<T> actual, ToIntFunction<? super T> function) {
        expected.sort(Comparator.comparingInt(function));
        actual.sort(Comparator.comparingInt(function));
        assertEquals(expected.size(), actual.size());

        IntStream.range(0, expected.size())
                .forEach(index -> assertEquals(expected.get(index), actual.get(index)));
    }
}
