package com.epam.enrolleesystem.utils;

import com.epam.enrolleesystem.model.Faculty;
import org.assertj.core.util.Sets;

public final class FacultyTestUtils {
    private FacultyTestUtils() {
    }

    public static Faculty createFacultyModel() {
        return (Faculty) new Faculty()
                .setName("testFaculty")
                .setAllPlaceCount(123)
                .setFundedPlaceCount(45)
                .setSubjects(Sets.newHashSet(SubjectTestUtils.createSubjectList()))
                .setId(1L);
    }
}
