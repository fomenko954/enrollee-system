package com.epam.enrolleesystem.utils;

import com.epam.enrolleesystem.model.Subject;
import com.google.common.collect.Lists;

import java.util.List;

public final class SubjectTestUtils {
    private SubjectTestUtils() {
    }

    public static Subject createSubjectModel() {
        return createSubject("testSub1", 1L);
    }

    public static List<Subject> createSubjectList() {
        return Lists.newArrayList(
                createSubject("testSub1", 1L),
                createSubject("testSub2", 2L),
                createSubject("testSub3", 3L));
    }

    private static Subject createSubject(String name, Long id) {
        return (Subject) new Subject()
                .setName(name)
                .setId(id);
    }
}
