package com.epam.enrolleesystem.utils;

import com.epam.enrolleesystem.domain.CompliantDto;
import com.epam.enrolleesystem.domain.RegistrationDto;
import com.epam.enrolleesystem.domain.ResponseWrapper;
import com.epam.enrolleesystem.domain.UserSubjectDto;
import com.epam.enrolleesystem.domain.UserSubjectList;
import com.epam.enrolleesystem.model.Faculty;
import com.epam.enrolleesystem.model.Role;
import com.epam.enrolleesystem.model.Subject;
import com.epam.enrolleesystem.model.User;
import com.epam.enrolleesystem.model.userfaculty.RegistrationStatus;
import com.epam.enrolleesystem.model.userfaculty.UserFaculty;
import com.epam.enrolleesystem.model.userfaculty.UserFacultyId;
import com.epam.enrolleesystem.model.usersubject.UserSubject;
import com.epam.enrolleesystem.model.usersubject.UserSubjectId;
import org.assertj.core.util.Lists;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class UserTestUtils {

    private UserTestUtils() {
    }

    public static User createUserModel() {
        return (User) new User()
                .setEmail("test@email.com")
                .setPassword("testPassword")
                .setFirstName("testFirstName")
                .setLastName("testLastName")
                .setMiddleName("testMiddleName")
                .setCity("testCity")
                .setRegion("testRegion")
                .setSchoolNumber(1)
                .setIsBlocked(false)
                .setRole(Role.ROLE_STUDENT)
                .setAvatarUrl("testAvater")
                .setCertificateUrl("testCertificate")
                .setId(1L);
    }

    public static RegistrationDto createUserDto() {
        return new RegistrationDto()
                .setEmail("test@email.com")
                .setPassword("testPassword")
                .setConfirmPassword("testPassword")
                .setFirstName("testFirstName")
                .setLastName("testLastName")
                .setRegion("testRegion")
                .setMiddleName("testMiddleName")
                .setCity("testCity")
                .setSchoolNumber(1)
                .setAvatar(new MockMultipartFile("avatar", "avatar.png", MediaType.IMAGE_PNG_VALUE,
                        "testAvatarContent".getBytes()))
                .setCertificate(new MockMultipartFile("certificate", "certificate.png",
                        MediaType.IMAGE_PNG_VALUE, "testCertificateContent".getBytes()));
    }

    public static CompliantDto createCompliantDto() {
        Faculty faculty = FacultyTestUtils.createFacultyModel();
        User user = createUserModel();
        return new CompliantDto(user.getId(), faculty.getId());
    }

    public static Set<UserSubject> createUserSubjects(List<Subject> subjects) {
        Set<UserSubject> linkedSubject = new HashSet<>();

        linkedSubject.add(createUserSubject(createUserModel(), subjects.get(0), 85));
        linkedSubject.add(createUserSubject(createUserModel(), subjects.get(1), 74));
        linkedSubject.add(createUserSubject(createUserModel(), subjects.get(2), 89));

        return Collections.unmodifiableSet(linkedSubject);
    }

    public static Set<UserSubject> createUserSubjects() {
        return createUserSubjects(SubjectTestUtils.createSubjectList());
    }

    public static ResponseWrapper<UserSubject> createUserSubjectsResponseWrapper() {
        return ResponseWrapper.<UserSubject>builder()
                .user(createUserModel())
                .data(Lists.newArrayList(createUserSubjects(SubjectTestUtils.createSubjectList())))
                .build();
    }

    public static Set<UserFaculty> createUserFaculties() {
        Faculty faculty = FacultyTestUtils.createFacultyModel();
        UserFaculty userFaculty = createUserFaculty(createUserModel(), faculty, RegistrationStatus.REGISTERED, calculateExpiredDate(), null, 85 + 74 + 89);
        return Collections.singleton(userFaculty);

    }

    public static ResponseWrapper<UserFaculty> createUserFacultiesResponseWrapper() {
        Faculty faculty = FacultyTestUtils.createFacultyModel();
        UserFaculty userFaculty = createUserFaculty(createUserModel(), faculty, RegistrationStatus.REGISTERED, calculateExpiredDate(), null, 85 + 74 + 89);
        return ResponseWrapper.<UserFaculty>builder()
                .user(createUserModel())
                .data(Collections.singletonList(userFaculty))
                .build();
    }

    public static UserSubjectList createUserSubjectListDto() {
        List<UserSubjectDto> userSubjectDtoList = Lists.newArrayList(
                new UserSubjectDto(1L, 85),
                new UserSubjectDto(2L, 74),
                new UserSubjectDto(3L, 89));
        return new UserSubjectList(userSubjectDtoList);
    }

    private static UserSubject createUserSubject(User user, Subject subject, Integer mark) {
        return new UserSubject(new UserSubjectId(user.getId(), subject.getId()), user, subject, mark);
    }

    private static UserFaculty createUserFaculty(User user, Faculty faculty, RegistrationStatus status, Date expiredDate, Date acceptedDate, Integer summaryMark) {
        return new UserFaculty(new UserFacultyId(user.getId(), faculty.getId()), user, faculty, status, expiredDate,
                acceptedDate, summaryMark);
    }

    public static Date calculateExpiredDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(Calendar.getInstance().getTimeInMillis()));
        calendar.add(Calendar.DAY_OF_MONTH, 14);
        return new Date(calendar.getTimeInMillis());
    }
}
