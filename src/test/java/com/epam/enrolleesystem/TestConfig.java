package com.epam.enrolleesystem;

import com.epam.enrolleesystem.converter.RegistrationDtoToUserConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class TestConfig {

    @Bean
    @Primary
    public ConversionService defaultConversionService(PasswordEncoder passwordEncoder) {
        DefaultConversionService conversionService = new DefaultConversionService();
        conversionService.addConverter(new RegistrationDtoToUserConverter(passwordEncoder));
        return conversionService;
    }
}
